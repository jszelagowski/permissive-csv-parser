
# Task description

Our customers frequently operate with large CSV files. The format is text-based,
so lots of tools and libraries can create files in CSV format, but also shell scripts
and other approaches create them, potentially with errors and inconsistencies like
wrongly quoted characters, encoding errors, etc. All of the available CSV parsers
somehow fall short of reading files with incorrect quoting, new line characters in
cells, NULL values or very large files.

This is why we are going to create our own scalable and resilient implementation.

## Implement CSV Parse

The aim is to write a CSV parser, with the input being a csv file. For a description
of CSV files, please see here: : https://en.wikipedia.org/wiki/Comma-separated_values.

Please take the following into consideration:

* The quoting string should be configurable, defaulting to `"`
* The line separator should also be configurable, defaulting to `\n`
* The field delimiter should also be configurable, defaulting to `,`
* Quoting, line separator and field delimiters can be strings with multiple characters
* The CSV can contain an optional header row
* The parser must not silently drop input data
* String values can be optionally quoted, enclosed in the predefined quoting character
* The parser should handle new line characters embedded in a quoted cell. The following
example should yield a csv with two lines, where the second field in the first line
contains a new line character
```
a,"a split
cell",b,"something else"
```
* The parser should handle field delimiters inside quoted cells. The following line
should yield exactly three fields:
`a,"b,c,d",e`
* Two consecutive field delimiters mean the value is absent/null. For example in
the line  the second field is missing, but the line `"a",,c` contains three values
* A field delimiter at the end of the line means the last value is absent/null.
For example, in the following line there are three fields: `a,b,`
* A field where part of the text appears as "quoted" and part as not quoted should
be interpreted as a single field. In the following example,three fields should be
read: `"abc,"onetwo,three,doremi`
* The input file can potentially be very large (>1TB) so the solution must handle
large data sets
* The focus of the solution is not on performance, but unnecessary performance
impacting processing should be avoided
* Attempt to handle files in arbitrary encodings other than UTF-8
* Focus on a functional immutable solution

Deliverables

* A project written in Scala
* CSV needs to be parsed via the API. The results can be simply printed out to the
console
* A Git project or ZIP file containing the CSV parser project

# Solution

## Design
The project implements solution utilizing akka streams. In its core is conversion from
a stream of characters into a stream of rows. That design allows for easy implementation
of different API functions (see `FileParser` for examples) and incorporating data from sources
other than local file system (e.g. socket, HTTP).

## Usage
See `FileParserTest` and `FileParser` for inspiration.

## Testing
```
sbt test
```

## Assumptions / known limitations
The list contains additional assumptions to make specification more explicit. Most of
the assumptions are easy to lift / change / implement if necessary.

* empty string is considered to be null value (None) no matter if quoted or not
* empty line is considered being a row containing single null cell (by analogy to
empty trailing cells)
* the cell starting with quoting mark is considered quoted till next occurrence
of quoting mark. The exception is made for double quoting mark which is
considered to be an escape mark of quoting string itself. The rest of the cell
after closing quoting mark is not quoted.
* no consistency checks for number of cells in a row are performed - rows with
different number of cells will be accepted
* byte-order mark (BOM) is not handled explicitly
* carriage return (CR) `\r` is not handled specially. This might be quite
unexpected if input contains `\r\n` as line breaks and `\n` is used as line separator.
* the behaviour in situations when any of line separator, delimiter or quoting string
are the same is not explicitly defined
* the behaviour in situations when any of line separator, delimiter or quoting string
is a prefix of any other of them is not explicitly defined
* every row of input data must fit into memory
* easy performance improvement - use string buffer for string manipulations - it will
not leak any mutable state through API anyway

# License
CSV Parser
Copyright (C) 2020  Jakub Szelagowski

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
