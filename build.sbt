name := "csv-parser"
version := "0.1"
scalaVersion := "2.13.2"

lazy val akkaVersion = "2.6.5"
lazy val easyMockVersion = "4.2"
lazy val scalaTestVersion = "3.1.0"

libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-actor" % akkaVersion,
  "com.typesafe.akka" %% "akka-stream" % akkaVersion,
  "com.typesafe.akka" %% "akka-testkit" % akkaVersion % Test,
  "com.typesafe.akka" %% "akka-stream-testkit" % akkaVersion % Test,

  "org.scalatest" %% "scalatest" % scalaTestVersion % Test,
  "org.easymock" % "easymock" % easyMockVersion  % Test,
  "org.scalatestplus" %% "easymock-3-2" % s"$scalaTestVersion.0"  % Test,
)
