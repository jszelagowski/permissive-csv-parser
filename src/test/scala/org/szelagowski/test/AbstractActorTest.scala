package org.szelagowski.test

import java.util.UUID

import akka.actor.ActorSystem
import akka.testkit.{ImplicitSender, TestKit}
import com.typesafe.config.{Config, ConfigFactory}
import org.scalatest.concurrent.Eventually
import org.scalatest.flatspec.AnyFlatSpecLike
import org.scalatest.matchers.should.Matchers
import org.scalatest.{BeforeAndAfterEach, OneInstancePerTest}

import scala.concurrent.duration._
import scala.jdk.CollectionConverters._

object AbstractActorTest {
  def config: Config = ConfigFactory.parseMap(Map(
    "akka.actor.guardian-supervisor-strategy" -> "akka.actor.StoppingSupervisorStrategy"
  ).asJava)
}

abstract class AbstractActorTest
  extends TestKit(ActorSystem(s"test-system-${UUID.randomUUID}", AbstractActorTest.config))
    with AnyFlatSpecLike with SavedEasyMockSugar with Matchers
    with BeforeAndAfterEach with OneInstancePerTest
    with ImplicitSender with Eventually {

  override implicit val patienceConfig: PatienceConfig = PatienceConfig(500.millis, 25.millis)

  override def afterEach(): Unit = {
    super.afterEach()
    system.terminate()
  }
}
