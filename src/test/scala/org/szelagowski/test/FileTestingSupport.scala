package org.szelagowski.test

import java.nio.file.{Files, Path}

trait FileTestingSupport {

  protected def usingTemporaryFile(filePrefix: String = s"${getClass.getSimpleName}-", fileSuffix: String = ".tmp")(func: Path => Unit): Unit =
    using(Files.createTempFile(filePrefix, fileSuffix), Files.delete)(func)

  protected def using[T, R](resource: T, close: T => Unit)(func: T => R): R =
    try func(resource) finally close(resource)
}
