package org.szelagowski.test

import org.easymock.EasyMock.createMockBuilder
import org.scalatestplus.easymock.EasyMockSugar

import scala.reflect.ClassTag

/**
 * This helper class helps not to forget to pass created mocks to whenExecuting() function
 */
trait SavedEasyMockSugar extends EasyMockSugar {

  private[this] var mocks = Set[AnyRef]()

  private[this] def saveMock[T <: AnyRef](mock: T): T = {
    mocks = mocks + mock
    mock
  }

  def mockAbstractMethods[T <: AnyRef](initArgs: AnyRef*)(implicit classTag: ClassTag[T]): T = {
    val mock = createMockBuilder(classTag.runtimeClass.asInstanceOf[Class[T]])
      .withConstructor(initArgs: _*)
      .createMock()
    saveMock(mock)
  }

  override def mock[T <: AnyRef](implicit classTag: ClassTag[T]): T = {
    val mock = super.mock[T](classTag)
    saveMock(mock)
  }

  override def strictMock[T <: AnyRef](implicit classTag: ClassTag[T]): T = {
    val mock = super.strictMock[T](classTag)
    saveMock(mock)
  }

  override def niceMock[T <: AnyRef](implicit classTag: ClassTag[T]): T = {
    val mock = super.niceMock[T](classTag)
    saveMock(mock)
  }

  override def whenExecuting(givenMocks: AnyRef*)(fun: => Unit): Unit = {
    val allMocks = mocks ++ givenMocks
    super.whenExecuting(allMocks.toList: _*)(fun)
  }

}
