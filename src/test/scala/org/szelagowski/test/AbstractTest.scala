package org.szelagowski.test

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers
import org.scalatest.{BeforeAndAfterEach, OneInstancePerTest}

abstract class AbstractTest extends AnyFlatSpec with SavedEasyMockSugar with Matchers
  with BeforeAndAfterEach with OneInstancePerTest
