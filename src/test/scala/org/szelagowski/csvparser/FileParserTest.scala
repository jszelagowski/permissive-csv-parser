package org.szelagowski.csvparser

import java.nio.file.Files

import akka.Done
import org.scalatest.concurrent.ScalaFutures
import org.szelagowski.csvparser.Parser.Row
import org.szelagowski.test.{AbstractActorTest, FileTestingSupport}

import scala.concurrent.Await
import scala.concurrent.duration._

class FileParserTest extends AbstractActorTest with FileTestingSupport with ScalaFutures {

  private val fileParser = new FileParser()

  it should "execute function foreach row" in usingTemporaryFile() { path =>
    val fileContent = "a,b,c\nd,e,f\ng,h,i"
    Files.writeString(path, fileContent)

    val handler = mock[Row => Unit]
    expecting {
      handler(List(Some("a"), Some("b"), Some("c"))).andReturn(()).once()
      handler(List(Some("d"), Some("e"), Some("f"))).andReturn(()).once()
      handler(List(Some("g"), Some("h"), Some("i"))).andReturn(()).once()
    }
    whenExecuting() {
      fileParser.foreachRow(path)(handler)
        .futureValue should be(Done)
    }
  }

  it should "fail future on handler error" in usingTemporaryFile() { path =>
    val fileContent = "a,b,c\nd,e,f\ng,h,i"
    Files.writeString(path, fileContent)

    val expectedException = new RuntimeException("Oh, no!")
    intercept[RuntimeException] {
      Await.result(fileParser.foreachRow(path)(_ => throw expectedException), 1.second)
    } should be(expectedException)
  }

  it should "execute function foreach row with header" in usingTemporaryFile() { path =>
    val fileContent = "a,b,c\nd,e,f\ng,h,i"
    Files.writeString(path, fileContent)

    val handler = mock[(Row, Row) => Unit]
    val header = List(Some("a"), Some("b"), Some("c"))

    expecting {
      handler(header, List(Some("d"), Some("e"), Some("f"))).andReturn(()).once()
      handler(header, List(Some("g"), Some("h"), Some("i"))).andReturn(()).once()
    }
    whenExecuting() {
      fileParser.foreachRowWithHeader(path)(handler)
        .futureValue should be(Done)
    }
  }

  // on purpose more showcase than test
  it should "add integer values in second column" in usingTemporaryFile() { path =>
    val fileContent = "a,3,c\nd,6,f\ng,11,i"
    Files.writeString(path, fileContent)

    fileParser.fold(path)(0)((acc, row) => acc + row(1).get.toInt)
      .futureValue should be(20)
  }
}
