package org.szelagowski.csvparser

import akka.stream.scaladsl.{Sink, Source}
import org.szelagowski.csvparser.Parser.Row
import org.szelagowski.test.AbstractActorTest

import scala.concurrent.Await
import scala.concurrent.duration._

class ParserTest extends AbstractActorTest {

  List(
    ("\n", ",", """""""),
    ("xxx", "yy", "zzzz"),
    ("\r\n", "woopie", "\\"),
  ).foreach { case (eol, delimiter, quote) =>

    val testNameSuffix = s"using line separator='$eol', delimiter='$delimiter', quote='$quote'"
    implicit val parser: Parser = new Parser(CsvFormat(eol, delimiter, quote))

    it should s"parse single line $testNameSuffix" in verify(
      input = s"aa${delimiter}bb${delimiter}cc",
      expected = List(
        List(Some("aa"), Some("bb"), Some("cc"))
      ))

    it should s"parse trivial multi line csv input $testNameSuffix" in verify(
      input = s"aa${delimiter}bb${delimiter}cc${eol}bb${delimiter}c${delimiter}ddd${eol}ccc${delimiter}d${delimiter}ee",
      expected = List(
        List(Some("aa"), Some("bb"), Some("cc")),
        List(Some("bb"), Some("c"), Some("ddd")),
        List(Some("ccc"), Some("d"), Some("ee")),
      ))

    it should s"handle empty values as none $testNameSuffix" in verify(
      input = s"aa$delimiter${delimiter}cc",
      expected = List(
        List(Some("aa"), None, Some("cc"))
      ))

    it should s"handle empty values everywhere $testNameSuffix" in verify(
      input = s"${delimiter}aa$delimiter${delimiter}cc$delimiter",
      expected = List(
        List(None, Some("aa"), None, Some("cc"), None)
      ))

    it should s"treat quoted empty strings as none as well $testNameSuffix" in verify(
      input = s"aa$delimiter$quote$quote${delimiter}cc",
      expected = List(
        List(Some("aa"), None, Some("cc"))
      ))

    it should s"accept properly quoted cells $testNameSuffix" in verify(
      input = s"${quote}aa$quote$delimiter${quote}bbb$quote$delimiter${quote}cc$quote",
      expected = List(
        List(Some("aa"), Some("bbb"), Some("cc"))
      ))

    it should s"escape double quotes in quoted text $testNameSuffix" in verify(
      input = s"a$delimiter${quote}bb$quote${quote}b$quote${delimiter}c",
      expected = List(
        List(Some("a"), Some(s"bb${quote}b"), Some("c"))
      ))

    it should s"preserve all quotes in non-quoted text $testNameSuffix" in verify(
      input = s"a${delimiter}bb$quote${quote}b${delimiter}c",
      expected = List(
        List(Some("a"), Some(s"bb$quote${quote}b"), Some("c"))
      ))

    it should s"preserve delimiters and line separators in quoted cells $testNameSuffix" in verify(
      input = s"a$delimiter${quote}hello${eol}world$quote$delimiter${quote}split${delimiter}me$quote",
      expected = List(
        List(Some("a"), Some(s"hello${eol}world"), Some(s"split${delimiter}me"))
      ))

    it should s"handle incorrect quotes $testNameSuffix" in verify(
      input = s"${quote}abc$delimiter${quote}onetwo${delimiter}three${delimiter}doremi",
      expected = List(
        List(Some(s"abc${delimiter}onetwo"), Some("three"), Some("doremi"))
      ))

    it should s"treat empty lines as rows with single empty cell $testNameSuffix" in verify(
      input = s"$eol$eol",
      expected = List(
        List(None),
        List(None),
        List(None),
      ))
  }

  private def verify(input: String, expected: List[Row])(implicit parser: Parser): Unit = {
    val future = Source(input)
      .via(parser.flow)
      .runWith(Sink.seq)
    Await.result(future, 5.seconds) should be(expected)
  }
}
