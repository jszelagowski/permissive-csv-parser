package org.szelagowski.csvparser

import akka.NotUsed
import akka.stream.scaladsl.{Flow, Source}

class Parser(csv: CsvFormat) {

  import Parser.MarkEof._
  import Parser.ReadingStates._
  import Parser._

  def flow: Flow[Char, Row, NotUsed] =
    Flow[Char]
      .map(Character)
      .concat(Source.single(Eof))
      .scan((NoEmit, ReadingNotQuotedCell: State, CurrentRow.empty)) {
        case ((_, state, currentRow), message) => message match {
          case Eof => (Some(currentRow.row), ReadingNotQuotedCell, CurrentRow.empty)
          case Character(char) => handle(state, currentRow.append(char))
        }
      }
      .mapConcat(_._1.toList)

  private def handle(state: State, row: CurrentRow) =
    state match {
      case ReadingNotQuotedCell => readingNotQuotedCell(row)
      case ReadingQuotedCell => readingQuotedCell(row)
      case EndOfQuote => endOfQuote(row)
    }

  private def readingNotQuotedCell(row: CurrentRow) =
    if (row.currentCell.contains(csv.quote)) (NoEmit, ReadingQuotedCell, row.copy(currentCell = None))
    else if (row.endsWith(csv.lineSeparator)) emitRow(row)
    else if (row.endsWith(csv.delimiter)) endOfCell(row)
    else (NoEmit, ReadingNotQuotedCell, row)

  private def readingQuotedCell(row: CurrentRow) =
    if (row.endsWith(csv.quote)) (NoEmit, EndOfQuote, row.strip(csv.quote))
    else (NoEmit, ReadingQuotedCell, row)

  private def endOfQuote(row: CurrentRow) =
    if (row.endsWith(csv.quote)) (NoEmit, ReadingQuotedCell, row) // escaped quoting sign in quoted text
    else if (row.endsWith(csv.lineSeparator)) emitRow(row)
    else if (row.endsWith(csv.delimiter)) endOfCell(row)
    else (NoEmit, EndOfQuote, row)

  private def endOfCell(row: CurrentRow) =
    (NoEmit, ReadingNotQuotedCell, row.strip(csv.delimiter).endOfCell)

  private def emitRow(row: CurrentRow) =
    (Some(row.strip(csv.lineSeparator).row), ReadingNotQuotedCell, CurrentRow.empty)
}

object Parser {
  type Row = List[Option[String]]
  private val NoEmit: Option[Row] = None

  private object MarkEof {

    sealed trait Message

    object Eof extends Message

    case class Character(char: Char) extends Message

  }

  private object ReadingStates {

    sealed trait State

    object ReadingNotQuotedCell extends State

    object ReadingQuotedCell extends State

    object EndOfQuote extends State

  }

  protected case class CurrentRow(previousCells: List[Option[String]], currentCell: Option[String]) {

    def append(char: Char): CurrentRow = copy(currentCell = Some(currentCell match {
      case None => char.toString
      case Some(str) => str + char
    }))

    def endsWith(suffix: String): Boolean = currentCell.exists(_.endsWith(suffix))

    def strip(suffix: String): CurrentRow = copy(
      currentCell = currentCell
        .map(_.dropRight(suffix.length))
        .flatMap(v => if (v.isEmpty) None else Some(v))
    )

    def endOfCell: CurrentRow = CurrentRow(previousCells = currentCell :: previousCells, currentCell = None)

    def row: Row = endOfCell.previousCells.reverse
  }

  object CurrentRow {
    def empty: CurrentRow = CurrentRow(Nil, None)
  }

}

