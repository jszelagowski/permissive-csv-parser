package org.szelagowski.csvparser

import java.nio.file.Path

import akka.stream.Materializer
import akka.stream.scaladsl.{Sink, Source}
import akka.{Done, NotUsed}
import org.szelagowski.csvparser.Parser.Row

import scala.concurrent.Future
import scala.io.Codec

class FileParser(implicit materializer: Materializer) {

  def foreachRow(path: Path)(rowHandler: Row => Unit)(implicit codec: Codec, csvConfig: CsvFormat): Future[Done] =
    source(path)
      .runWith(Sink.foreach(rowHandler))

  def fold[U](path: Path)(zero: U)(f: (U, Row) => U)(implicit codec: Codec, csvConfig: CsvFormat): Future[U] =
    source(path)
      .runWith(Sink.fold(zero)(f))

  def foreachRowWithHeader(path: Path)(rowHandler: (Row, Row) => Unit)(implicit codec: Codec, csvConfig: CsvFormat): Future[Done] =
    fold[Option[Row]](path)(None) { (maybeHeader, row) =>
      Some(maybeHeader.fold(row) { header =>
        rowHandler(header, row)
        header
      })
    }
      .map(_ => Done)(materializer.executionContext)

  private def source(path: Path)(implicit codec: Codec, csvConfig: CsvFormat): Source[Row, NotUsed] =
  // Using scala.io.Source.fromFile() instead of FileIO.fromPath() to avoid splitting
  // multibyte characters between two buffers. The case SHOULD be tested still
    Source.fromIterator(() => scala.io.Source.fromFile(path.toFile)) // TODO: test multibyte handling with different charsets and very small buffer
      .via(new Parser(csvConfig).flow)
}
