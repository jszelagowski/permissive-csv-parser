package org.szelagowski.csvparser

case class CsvFormat(lineSeparator: String, delimiter: String, quote: String)

object CsvFormat {
  implicit val defaultFormat: CsvFormat = CsvFormat(lineSeparator = "\n", delimiter = ",", quote = """"""")
}
